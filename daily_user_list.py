# daily_user_list.py v1.2
# written by: Phil Cowger
# last udpated: 10/19/2017
# This script will perform a mysql query for the previous day's unique user logins
# to the RiskIQ platform, export that list to a .csv, and upload that .csv to Gainsight

import mysql.connector
import json
import collections
from datetime import date, time
import datetime
import dateutil.parser
import csv
import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder
import os

CONFIG = {
    'user':'linkco_readonly',
    'password': 'l1nk0',
    'host': 'internal-usw2-mysql-slaves-698196514.us-west-2.elb.amazonaws.com',
    'database': 'linkco_prod',
    'port': '3306',
    'connection_timeout': 20
}

def get_user_list():
#SQL query to get list of unique user logins from the previous day that were not from riskiq
    count = 1
    while True:
        try:
            print " - CNX attempt # " + str(count)
            update = datetime.datetime.now() - datetime.timedelta(1)
            CNX = mysql.connector.connect(**CONFIG)
            cursor = CNX.cursor(dictionary=True)
            query = ("""
                SELECT ws.salesForceID, ur.email, DATE_FORMAT(ur.createDatetime,'%%Y-%%m-%%d') AS createDate, DATE_FORMAT(ur.lastLogin,'%%Y-%%m-%%d') AS lastLoginDate
                FROM workspaces AS ws
                	JOIN userhasworkspace AS uhw ON uhw.workspaceID = ws.workspaceID
                	JOIN riskiquser AS ur ON uhw.userID = ur.userID
                WHERE ws.salesForceID IS NOT NULL
                	AND ws.salesForceID != ''
                    AND ur.statusCode = 'Y'
                	AND ur.email IS NOT NULL
                	AND ws.demo = 0
                	AND ws.test = 0
                	AND ur.email IS NOT NULL
                	AND ur.email != ''
                	AND ur.email NOT LIKE '%%@riskiq.net'
                	AND ur.email NOT LIKE '%%@riskiq.com'
                	AND ur.lastLogin IS NOT NULL
                	AND ur.lastLogin >= '%(x)s'
                    AND ur.lastLogin < '%(y)s'
                GROUP BY ur.userID""" % {'x':update.strftime("%Y-%m-%d"),'y':datetime.datetime.now().strftime("%Y-%m-%d")}
            )
            cursor.execute(query)
            u_list = []
            for u in cursor:
                u_rec = {}
                u_rec["salesForceID"] = u['salesForceID']
                u_rec["email"] = u['email']
                u_rec["createDate"] = str(u['createDate'])
                u_rec["lastLoginDate"] = str(u['lastLoginDate'])
                u_list.append(u_rec)
            cursor.close()
            CNX.close()
            return u_list
        except Exception as e:
            count += 1
            print str(e)
            continue
        break


def export_csv(u_list):
#Creates .csv file formatted for upload to Gainsight
    u_parsed = json.loads(json.dumps(u_list))
    json.dumps(u_parsed, indent=4)
    f = csv.writer(open("user_list.csv", "wb+"))
    f.writerow(["Account ID", "Email", "lastLoginDate", "createDate"])
    for x in u_parsed:
        f.writerow([x["salesForceID"],
            x["email"],
            x["lastLoginDate"],
            x["createDate"]])

def gainsight_api():
#Grabs the previously created .csv file and uploads it to Gainsight
    m = MultipartEncoder(
        fields={
            "jobId":"36f9cf7b-f980-4e5b-abe6-7925178623cb",
            "file": ("user_list.csv", open("user_list.csv", "rb"), "text/plain")
        }
    )
    url = "https://app.gainsight.com/v1.0/admin/connector/job/bulkimport"
    headers = {
        "Content-Type": m.content_type,
        "loginName": "chris.duckers@riskiq.net",
        "appOrgId": "00Di0000000JuXdEAK",
        "accessKey": "4dee9b32-0f87-4596-88d4-44cba521e638",
    }
    try:
        response = requests.post(url,data=m,headers=headers)
        print response.status_code
        parsed_result = json.loads(response.text)
        print json.dumps(parsed_result, indent=4)
    except Exception as e:
        print str(e)

def main():
#main function call
    try:
    #removes any previous/existing .csv files
        os.remove('user_list.csv')
    except Exception as e:
        print str(e)
    u_list = get_user_list()
    #print json.dumps(u_list, indent=4) #debug output
    update = datetime.datetime.now() - datetime.timedelta(1)
    print " # of users for " + update.strftime("%Y-%m-%d") + ": " + str(len(u_list)) #debug output
    export_csv(u_list)
    gainsight_api()

main()
